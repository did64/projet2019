var app = new Vue({
    el: '#app',
    data:{
      apiBase: 'http://localhost:8000/api/',
      cours: [],
      salles : [],
      emptyCours : false,
      options: { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' },
      date : new Date().toLocaleDateString('fr-FR'),
      dateFr: new Date().toLocaleDateString('fr-FR', { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' })
    },
    methods:{
      getCourOfTheDay(){
        var route = this.routeFormat(this.date);
        axios.get(this.apiBase + 'courDate/' + route)
        .then(response => {
            response.data.forEach((d)=>{
              var debut = new Date(d.dateHeureDebut);
              var fin = new Date(d.dateHeureFin);
              var cour = {
                'salle' : d.salle,
                'date' : debut.toLocaleDateString('fr-FR', this.options),
                'dateHeureDebut' : debut.toLocaleTimeString('fr-FR'),
                'dateHeureFin' : fin.toLocaleTimeString('fr-FR'),
                'type' : d.type,
                'professeur': d.professeur,
                'profId': d.profId,
                'matiere' : d.matiere
              }
              this.cours.push(cour);
              if(!this.salles.includes(d.salle)){
                this.salles.push(d.salle);
              }
            })    
        })
        .catch(error => {
          console.log(error);
        })
        .then( ()=> {
          this.salles.sort(this.compare);
          this.salles.reverse();
          if(this.cours.length == 0){
            this.emptyCours = true;
          }  
        })
      },
      routeFormat(date){
        var currentDate = date.split('/');
        var newFormat = currentDate[2]+'-'+currentDate[1]+'-'+currentDate[0];
        return newFormat;
      },
      changeDay(daySetter){
        this.cours = [];
        this.salles = [];
        var date = this.routeFormat(this.date);
        var day = new Date(date);
        var dayAfter = day.setDate(day.getDate() + daySetter);
        this.date = new Date(dayAfter).toLocaleDateString('fr-FR');
        this.dateFr = new Date(dayAfter).toLocaleDateString('fr-FR', this.options)
        var route = this.routeFormat(this.date);
        axios.get(this.apiBase + 'courDate/' + route)
        .then(response => {
            response.data.forEach((d)=>{
              var debut = new Date(d.dateHeureDebut);
              var fin = new Date(d.dateHeureFin);
              var cour = {
                'salle' : d.salle,
                'date' : debut.toLocaleDateString('fr-FR', this.options),
                'dateHeureDebut' : debut.toLocaleTimeString('fr-FR'),
                'dateHeureFin' : fin.toLocaleTimeString('fr-FR'),
                'type' : d.type,
                'professeur': d.professeur,
                'profId': d.profId,
                'matiere' : d.matiere
              }
              this.cours.push(cour);
              if(!this.salles.includes(d.salle)){
                this.salles.push(d.salle);
              }
              this.emptyCours = false;
            })
            
        })
        .catch(error => {
          console.log(error);
        })
        .then( ()=> {
          this.salles.sort(this.compare);
          this.salles.reverse();
          if(this.cours.length == 0){
            this.emptyCours = true;
          }  
        })
      },
      compare(x,y){
        return y - x;
      }
    },
    mounted(){
      this.getCourOfTheDay();

    },
});