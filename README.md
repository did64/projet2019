# Travail de sylvie NOLAIS et didier LEVEQUE

# Back-end

## Entity
### Cours

* $dateHeureDebut: @Assert\GreaterThan("+ 1 hours"): la date doit être supérieur à la l'heure actuelle + 1.
* @Assert\Expression("this.dansOuverture()",message="l'heure est en dehors des heures d'ouverture") : retourne message d'erreur si l'heure saisie est avant 8h ou apres 18h.
* $dateHeureFin: @Assert\Expression("value > this.getDateHeureDebut()"): doit être après le début du cours.
* $type: @Assert\Choice({"TD", "TP", "Cours"}): 3 choix possibles.
* $salle: @Assert\Expression("this.salleDispo()",message="Cette salle est déjà prise"):  vérifie si la salle est déjà prise et retourne un message d'erreur si elle est occupée.
* $professeur: @Assert\Expression("this.profDispo()",message="Ce professeur est déjà en cours"): vérifie que le professeur n'est pas déjà pris et retourne un message d'erreur si il est déjà en cours.

### Salle
* $numero: @Assert\GreaterThan(0): numéro doit être positif.


## Api
### CoursController.php
* fonction getCours: récupère la liste de tous les cours.
             getCour($id): récupère le cours de l'id passé en paramètre.
             getCourDate($date): récupère la liste des cours de la date passée en parametre au format Y-m-d.

### SalleController.php
* fonction getSalles: récupère la liste des salles.


## Repository
### CoursRepository.php
* Ajout de la fonction findByDate($date): récupère la liste des cours de la date passée en paramètre.

# Front-end

## edt.js

## Les variables
* cours et salles contiennent la liste des cours et des salles occupées pour le jour courant.
* emptyCours est un booléen qui indique si des cours existe sur la journée selectionnée. 
* date et dateFR stocke la date courante au format chiffre, et au format texte au moyen des options.

## Les méthodes

* getCourOfTheDay : récupère les données des cours du jour et les affecte à l'objet cour. Le tableau salles stocke les salles occupées du jour en vérifiant de les stocker qu'une seule fois.

* routeFormat : retourne une date formatée pour l'appel à l'api.

* changeDay : La méthode est appelé lors d'un click, elle réinitialise les variables, modifie la date du jour à + 1 ou - 1 , formate cette date et fait l'appel à l'api. Ensuite le traitement est le même que getCourOfTheDay.

* compare : cette méthode sert d'ptions à la méthode native sort pour trier mon tableau de salles.

* Dans le mounted, qui est appelé au chargement de la page, j'appel getCourOfTheDay pour charger les cour du jour courant.

Plusieurs routes ont été développées coté back, mais finalement nous nous servons içi que d'une seule. Cependant elles pourront toujours servir dans le futur.

# Petit plus...

### Nous avons écrit des tests qui s'exécutent avec succès pour le CoursController.

### Nous avons relié le professeur de chaque cour à la page qui permet de lui attribuer une note. Au moyen d'un parametre dans l'url qui appelle directement la méthode recupererAvis dans avis.html, avec l'id du professeur concerné passé en parametre.
