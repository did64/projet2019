<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CoursRepository")
 * @UniqueEntity(
 *     fields={"salle", "dateHeureDebut", "dateHeureFin"},
 *     errorPath="salle",
 *     message="Cette salle est déjà occupée"
 * )
 */
class Cours
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\GreaterThan("+1 hours")
     * @Assert\Expression("this.dansOuverture()",message="l'heure est en dehors des heures d'ouverture")
     */
    private $dateHeureDebut;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\Expression("value > this.getDateHeureDebut()")
     * @Assert\Expression("this.dansOuverture()",message="l'heure est en dehors des heures d'ouverture")
     */
    private $dateHeureFin;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Choice({"TD", "TP", "Cours"})
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Professeur", inversedBy="cours")
     * @Assert\Expression("this.profDispo()",message="Ce professeur est déjà en cours")
     */
    private $professeur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Matiere", inversedBy="cours")
     */
    private $matiere;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Salle", inversedBy="cours")
     * @Assert\Expression("this.salleDispo()",message="Cette salle est déjà prise")
     */
    private $salle;

    public function __toString() :string {
        return $this->type;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateHeureDebut(): ?\DateTimeInterface
    {
        return $this->dateHeureDebut;
    }

    public function setDateHeureDebut(\DateTimeInterface $dateHeureDebut): self
    {
        $this->dateHeureDebut = $dateHeureDebut;

        return $this;
    }

    public function getDateHeureFin(): ?\DateTimeInterface
    {
        return $this->dateHeureFin;
    }

    public function setDateHeureFin(\DateTimeInterface $dateHeureFin): self
    {
        $this->dateHeureFin = $dateHeureFin;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }


    public function getProfesseur(): ?Professeur
    {
        return $this->professeur;
    }

    public function setProfesseur(?Professeur $professeur): self
    {
        $this->professeur = $professeur;

        return $this;
    }

    public function getMatiere(): ?Matiere
    {
        return $this->matiere;
    }

    public function setMatiere(?Matiere $matiere): self
    {
        $this->matiere = $matiere;

        return $this;
    }

    public function getSalle(): ?Salle
    {
        return $this->salle;
    }

    public function setSalle(?Salle $salle): self
    {
        $this->salle = $salle;

        return $this;
    }

    public function toArray()
    {
        return [
        'id'    => $this->getId(),
        'type' => $this->getType(),
        'dateHeureDebut'   => $this->getDateHeureDebut(),
        'dateHeureFin' => $this->getDateHeureFin(),
        'salle' => $this->getSalle()->getNumero(),
        'professeur' => $this->getProfesseur()->getNom(),
        'profId' => $this->getProfesseur()->getId(),
        'matiere' => $this->getMatiere()->getTitre()
        ];
    }

    public function dansOuverture(){
      $horaireNouveauDebut = $this->getDateHeureDebut();
      $horaireNouveauFin = $this->getDateHeureFin();
      $horaireOuvertureDebut = new \DateTime($horaireNouveauDebut->format("Y-m-d")."07:59:00");
      $horaireOuvertureFin = new \DateTime($horaireNouveauFin->format("Y-m-d")."18:01:00");
      if($horaireNouveauDebut > $horaireOuvertureDebut && $horaireNouveauDebut < $horaireOuvertureFin && $horaireNouveauFin > $horaireOuvertureDebut && $horaireNouveauFin < $horaireOuvertureFin){
         return true;
       }
       return false;
    }

    public function salleDispo(){
        $horaireNouveauDebut = $this->getDateHeureDebut();
        $horaireNouveauFin = $this->getDateHeureFin();
        $salle = $this->getSalle();
        $salleCours = $salle->getCours();
        $salleDispo = true;

        foreach ($salleCours as $uneSalleCours) {
            if($uneSalleCours->getId() != $this->getId()) {
                $salleCoursDebut = $uneSalleCours->getDateHeureDebut();
                $salleCoursFin = $uneSalleCours->getDateHeureFin();
                $salleDispo = ($horaireNouveauDebut <= $salleCoursDebut && $horaireNouveauFin <= $salleCoursDebut) || ($horaireNouveauDebut >= $salleCoursFin && $horaireNouveauFin >= $salleCoursFin);
                if (!$salleDispo) return $salleDispo;
            }
        }
        return $salleDispo;
    }


    public function profDispo(){
       $nouveauCoursDebut = $this->getDateHeureDebut();
       $nouveauCoursFin = $this->getDateHeureFin();
       $professeur = $this->getProfesseur();
       $coursProfesseur = $professeur->getCours();
       $profDispo = true;
       foreach ($coursProfesseur as $unCoursProfesseur) {
          if($unCoursProfesseur->getId() != $this->getId()) {
               $coursProfesseurDebut = $unCoursProfesseur->getDateHeureDebut();
               $coursProfesseurFin = $unCoursProfesseur->getDateHeureFin();
               $profDispo = ($nouveauCoursDebut <= $coursProfesseurDebut && $nouveauCoursFin <= $coursProfesseurDebut) || ($nouveauCoursDebut >= $coursProfesseurFin && $nouveauCoursFin >= $coursProfesseurFin);
               if (!$profDispo) return $profDispo;
           }
       }
       return $profDispo;
    }

}

