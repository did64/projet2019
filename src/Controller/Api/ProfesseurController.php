<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Professeur;
use App\Entity\Avis;

class ProfesseurController extends AbstractController
{
    /**
     * @Route("/api/professeurs", name="api_get_professeurs", methods={"GET"})
     *
     */
    public function getProfesseurs(){

        $em = $this->getDoctrine()->getManager();
        $professeurs = $em->getRepository(Professeur::class)->findAll();

        $professeursList = [];

        foreach ($professeurs as $professeur){
            $professeursList[] = $professeur->toArray();
        }
    /*
        Method 1 : json_encode
        $response = new Response();
        $response->setContent(json_encode($professeursList));
        $response->headers->set('content-Type', 'application/json');
        $response->setStatusCode(200);


        return $response;
    */
        //Method 2 json()
        return $this->json($professeursList, 200);

        //Method 3 symfony serializer
        //return $this->json($professeurs, 200, [], ['ignored_attributes' => ['professeurs', 'professeur']]);

    }

    /**
     * @Route("/api/professeur/{id}", name="api_get_professeur", methods={"GET"})
     */
    public function getProfesseur(int $id){

        $em = $this->getDoctrine()->getManager();
        $professeur = $em->getRepository(Professeur::class)->find($id);

        if(is_null($professeur)){
            return $this->json(['message' => 'ce professeur est introuvable'], 404);
        }
        return $this->json($professeur->toArray(), 200);
    }

    /**
     * @Route("api/professeur/{id}/avis", name="api_get_professeur_avis", methods={"GET"})
     */
    public function getProfesseurAvis(int $id){

        $em = $this->getDoctrine()->getManager();
        $professeur = $em->getRepository(Professeur::class)->find($id);

        if(is_null($professeur)){
            return $this->json(['message' => 'ce professeur est introuvable'], 404);
        }
        return $this->json($professeur->getavisArray(), 200);

    }

    /**
     * @Route("api/professeur/{id}/avis", name="api_add_professeur_avis", methods={"PUT"})
     */
    public function addProfesseurAvis(Request $request , int $id){

        $em = $this->getDoctrine()->getManager();
        $professeur = $em->getRepository(Professeur::class)->find($id);

        if(is_null($professeur)){
            return $this->json(['message' => 'ce professeur est introuvable'], 404);
        }

        $data = json_decode($request->getContent(), true);

        $form = $this->createFormBuilder(new Avis(), ['csrf_protection' => false])
                ->add('note')
                ->add('commentaire')
                ->add('emailEtudiant')
                ->getForm();
        $form->submit($data);
        if(! $form->isValid()){
            return $this->json($this->getFormErrors($form), 400);
        }

        $avis = $form->getData();
        $avis->setProfesseur($professeur);
        $em->persist($avis);
        $em->flush();

        return $this->json($avis->toArray(), 201);

    }

    /**
     * @Route("api/avis/{id}", name="api_delete_avis", methods={"DELETE"})
     */
    public function deleteAvis(int $id){

        $em = $this->getDoctrine()->getManager();
        $avis = $em->getRepository(Avis::class)->find($id);

        if(is_null($avis)){
            return $this->json(['message' => 'cet avis est introuvable'], 404);
        }

        $em->remove($avis);
        $em->flush();

        return $this->json(null, 204);
    }


    private function getFormErrors(\Symfony\Component\Form\Form $form){

        $errors = [];

        foreach ($form->getErrors() as $error){
            $errors[$form->getName()][] = $error->getMessage();

        }

        foreach ($form as $child){
            if(! $child->isValid()){
                foreach ($child->getErrors() as $error){
                    $errors[$child->getName()][] = $error->getMessage();
                }
            }
        }

        return $errors;

    }


}
