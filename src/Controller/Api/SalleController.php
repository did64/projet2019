<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Salle;

class SalleController extends AbstractController
{
  /**
   * @Route("/api/salles", name="api_get_salles", methods={"GET"})
   *
   */
  public function getSalles(){

      $em = $this->getDoctrine()->getManager();
      $salles = $em->getRepository(Salle::class)->findAll();

      $salleList = [];

      foreach ($salles as $salle){
          $salleList[] = $salle->toArray();
      }

      return $this->json($salleList, 200);
    }
}
