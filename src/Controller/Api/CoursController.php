<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Cours;
use App\Entity\Salle;
use App\Entity\Matiere;

class CoursController extends AbstractController
{
  /**
   * @Route("/api/cours", name="api_get_cours", methods={"GET"})
   *
   */
  public function getCours(){

      $em = $this->getDoctrine()->getManager();
      $cours = $em->getRepository(Cours::class)->findAll();

      $coursList = [];

      foreach ($cours as $unCours){
          $coursList[] = $unCours->toArray();
      }
      
      return $this->json($coursList, 200);
    }

    /**
     * @Route("/api/cour/{id}", name="api_get_cour", methods={"GET"})
     */
    public function getCour($id){

        $em = $this->getDoctrine()->getManager();
        $cour = $em->getRepository(Cours::class)->find($id);

        if(is_null($cour)){
            return $this->json(['message' => 'Il n\'a pas de cours'], 404);
        }
        return $this->json($cour->toArray(), 200);
    }

    /**
     * @Route("/api/courDate/{date}", name="api_get_cour_date", methods={"GET"})
     */
    public function getCourDate($date){

        $em = $this->getDoctrine()->getManager();
        $cour = $em->getRepository(Cours::class)->findByDate($date);

        $coursList = [];

        foreach ($cour as $cours){
            $coursList[] = $cours->toArray();
        }

        return $this->json($coursList, 200);
    }

}
