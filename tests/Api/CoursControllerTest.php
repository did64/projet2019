<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Entity\Professeur;
use App\Entity\Cours;
use App\Entity\Matiere;
use App\Entity\Salle;

class CoursControllerTest extends WebTestCase
{
  private $client;
  private $em;

  public function setUp() {
      $this->client = static::createClient();
      $this->em = $this->client->getContainer()->get('doctrine.orm.entity_manager');
      $this->loadFixtures();
  }

  private function loadFixtures() {
      $this->tearDown();

      $professeur = new Professeur();
      $professeur->setEmail('xavier.godart@gmail.com');
      $professeur->setNom('Godart');
      $professeur->setPrenom('Xabi');
      $this->em->persist($professeur);

      $matiere = new Matiere();
      $matiere->setTitre('Prog Web av');
      $matiere->setReference('PWA 1203');
      $this->em->persist($matiere);

      $salle =new Salle();
      $salle->setNumero(1);
      $this->em->persist($salle);

      $cours = new Cours();
      $cours->setDateHeureDebut(new \DateTime('2020-01-01T15:03:01.012345Z'));
      $cours->setDateHeureFin(new \DateTime('2020-01-01T16:03:01.012345Z'));
      $cours->setType('TP');
      $cours->setProfesseur($professeur);
      $cours->setMatiere($matiere);
      $cours->setSalle($salle);
      $this->em->persist($cours);

      $this->em->flush();
  }

    public function testGetCours()
    {
        $this->client->request('GET', '/api/cours');

        $this->assertEquals(
            200,
            $this->client->getResponse()->getStatusCode()
            
        );

        $this->assertTrue(
            $this->client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'the "Content-Type" header is "application/json"' // optional message shown on failure
        );
        $this->assertTrue(true);
    }

    public function testGetCour()
    {
        $this->client->request('GET', '/api/cour/1');

        $this->assertEquals(
            200,
            $this->client->getResponse()->getStatusCode()
            
        );

        $this->assertTrue(
            $this->client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'the "Content-Type" header is "application/json"' // optional message shown on failure
        );
        $this->assertTrue(true);
    }

    public function testGetCourDate()
    {
        $this->client->request('GET', '/api/courDate/2020-01-01');

        $this->assertEquals(
            200,
            $this->client->getResponse()->getStatusCode()
            
        );

        $this->assertTrue(
            $this->client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'the "Content-Type" header is "application/json"' // optional message shown on failure
        );
        $this->assertTrue(true);
    }

    public function tearDown()
    {
        $this->purgeTable('App:Professeur');
        $this->purgeTable('App:Salle');
        $this->purgeTable('App:Matiere');
        $this->purgeTable('App:Cours');
    }
 

    private function purgeTable($className)
    {
        $classMetaData = $this->em->getClassMetadata($className);
        $connection = $this->em->getConnection();
        $dbPlatform = $connection->getDatabasePlatform();
        $connection->beginTransaction();
        try {
            $connection->query('SET FOREIGN_KEY_CHECKS=0');
            $q = $dbPlatform->getTruncateTableSql($classMetaData->getTableName());
            $connection->executeUpdate($q);
            $connection->query('SET FOREIGN_KEY_CHECKS=1');
            $connection->commit();
        }
        catch (\Exception $e) {
            $connection->rollback();
        }
    }
}
